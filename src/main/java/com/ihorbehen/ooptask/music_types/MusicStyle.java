package com.ihorbehen.ooptask.music_types;

public enum MusicStyle {
    CLASSIC, ROCK, JAZZ, FOLK, POP;
}
