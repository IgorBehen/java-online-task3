package com.ihorbehen.ooptask.music_types;

import java.util.List;

public class Song extends SongInfo {
    private MusicStyle musicStyle;
    private List<Song> list;

    public Song(int songId, String songName, String songAutor, double songDuration, MusicStyle musicStyle) {
        super(songId, songName, songAutor, songDuration);
        this.musicStyle = musicStyle;
    }

    public List<Song> getList() {
        return list;
    }

    public void setList(List<Song> list) {
        this.list = list;
    }

    public MusicStyle getMusicStyle() {
        return musicStyle;
    }

    public void setMusicStyle(MusicStyle musicStyle) {
        this.musicStyle = musicStyle;
    }

    @Override
    public String toString() {
        String song = super.toString() + " / " + " Music Style: " + musicStyle;
        return song;
    }
}
