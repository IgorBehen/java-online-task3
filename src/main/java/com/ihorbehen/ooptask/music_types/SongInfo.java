package com.ihorbehen.ooptask.music_types;

public abstract class SongInfo {
    private int songId;
    private double songDuration;
    private String songAutor;
    private String songName;

    public SongInfo(int songId, String songName, String songAutor, double songDuration) {
        this.songId = songId;
        this.songDuration = songDuration;
        this.songAutor = songAutor;
        this.songName = songName;
    }

    public SongInfo(int songId, String songName, double songDuration) {
        this.songId = songId;
        this.songDuration = songDuration;
        this.songName = songName;
    }

    public String toString() {
        return "id:" + songId + " / " + " Song Name: " + songName + " / "
                + " Song Autor: " + songAutor + " / " + " Song Duration: " + songDuration;
    }

    public int getSongId() {
        return songId;
    }

    public void setSongId(int id) {
        this.songId = id;
    }

    public double getSongDuration() {
        return songDuration;
    }

    public void setSongDuration(double duration) {
        this.songDuration = duration;
    }

    public String getSongAutor() {
        return songAutor;
    }

    public void setSongAutor(String songAutor) {
        this.songAutor = songAutor;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

}
