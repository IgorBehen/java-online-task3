package com.ihorbehen.ooptask;

import com.ihorbehen.ooptask.menu.Menu;
import com.ihorbehen.ooptask.song_manager.SongManager;

/**
 * OOP TASK (with classes, inheritance, polymorphism, encapsulation)
 * На свій вибір вибрати один з варіантів задачі:
 * https://learn.by/courses/course-
 * v1:EPAM+JC+ext1/courseware/ce87126be08849fbaad518740daf7dff/b736592550354a
 * a09e09a5ffe4fddde4/1
 * Також для цієї задачі створіть консольне меню, використовуючи ENUM.
 */

public class Main {
    public static void main(String[] args) {
        SongManager sm = new SongManager();
        sm.initData();
        Menu.menu();
    }
}
