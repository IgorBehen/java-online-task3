package com.ihorbehen.ooptask.menu;

import com.ihorbehen.ooptask.song_manager.SongManager;

import java.util.Scanner;

public enum Menu {
    FINISH,
    SORT,
    SEARCH,
    CALCULATE,
    BURN;

    public static void menu() {
        SongManager smEnum = new SongManager();
        Scanner scan = new Scanner(System.in);
        int x = 0;
        String s = "";
        Menu number = null;

        while (!"0".equals(s)) {
            System.out.println("1. To sort by style, enter                            - 1");
            System.out.println("2. For search song by duration, enter                 - 2");
            System.out.println("3. To calculate the duration of the collection, enter - 3");
            System.out.println("4. For burning collection to the disk, enter          - 4");
            System.out.println("0. To finish, press                                   - 0");
            s = scan.next();

            try {
                x = (Integer.parseInt(s));
                number = Menu.values()[x];

            } catch (NumberFormatException e) {
                System.out.println("Invalid input.");
            }
            if (number != null) {
                switch (number) {
                    case FINISH:
                        System.out.println("Goodbye!");
                        break;
                    case SORT:
                        smEnum.sortingByStyle();
                        break;
                    case SEARCH:
                        smEnum.SearchSongByDuration();
                        break;
                    case CALCULATE:
                        smEnum.CalculateTheDurationOfTheCollection();
                        break;
                    case BURN:
                        smEnum.BurnTheCollectionToDisk();
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
            }
        }
        scan.close();
    }
}
