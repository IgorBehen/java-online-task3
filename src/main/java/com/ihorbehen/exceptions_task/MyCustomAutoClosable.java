package com.ihorbehen.exceptions_task;

public class MyCustomAutoClosable implements AutoCloseable {

    public void working() {
        System.out.println("MyCustomAutoClosable worked!");
    }

    @Override
    public void close() throws Exception {
        throw new NullPointerException();
        //System.out.println("MyCustomAutoClosable closed!");
    }
}
