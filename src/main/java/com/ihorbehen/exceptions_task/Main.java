package com.ihorbehen.exceptions_task;

public class Main {

    public static double income(int coefficient) throws MyCustomException {
        double d;

        try {
            if ((d = 10 - 100 / coefficient) < 0)
                throw new MyCustomException("Negative income!");
            else return d;
        } catch (ArithmeticException e) {
            throw new MyCustomException("Div by zero!", e);
        }
    }

    private static void myCustomAutoClosable() throws Exception {
        try (MyCustomAutoClosable myAutoClosable = new MyCustomAutoClosable()) {
            myAutoClosable.working();
        }
    }

    public static void main(String[] args) throws Exception {
        try {
            double result = income(3); //or 0
        } catch (MyCustomException e) {
            System.err.println(e.toString());
            System.err.println(e.getСovertException());
        }

        Main.myCustomAutoClosable();
    }

}


