package com.ihorbehen.exceptions_task;

public class MyCustomException extends Exception {

    private Exception covert;

    public MyCustomException(String er) {
        super(er);
    }

    public MyCustomException(String er, Exception e) {
        super(er);
        covert = e;
    }

    public Exception getСovertException() {
        return covert;
    }


}



